/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });

var height = $('#doc-img').attr('data-height');
var template_id = $('#rules-data').attr('data-template-id');
var timeOutNotif = 500;
var colArray = [];
var colCords = [];
var currentCol = 0;
var container;

(function () {
    renderEditData();
    var type = $('#form-rule').data('type');
    if (type === 'create') {
        container = '.img-container';
    } else {
        container = '.img-container-edit';
    }
})();

$.fn.dataTable.render.ellipsis = function () {
    return function (data, type, row) {
        return type === 'display' && data.length > 10 ?
            data.substr(0, 10) + '…' :
            data;
    }
};

var documentsTable = $('#documents-data').DataTable({
    ajax: '/api/documents',
    columnDefs: [
        {
            "targets": 0,
            "data": "document_name",
            "render": function (data, type, row, meta) {
                return '<div class="form-check"><input class="form-check-input" type="checkbox" data-id="' + row.id + '"></div>';
            },
            "width": 10
        },
        {
            "targets": 1,
            "data": "document_name",
            "render": $.fn.dataTable.render.ellipsis()
        },
        {
            "targets": 2,
            "data": "hasTemplate",
            "render": function (data, type, row, meta) {
                if(row.hasTemplate === false) {
                    return '-';
                }
                return '<a href="/documents/template/' + row.templates[0].id + '">' + row.templates[0].template_name + '</a>';
            }
        },
        {
            "targets": 3,
            "data": "status",
            "render": function (data, type, row, meta) {
                if (row.status === 'scanned') {
                    return '<span class="badge badge badge-success text-uppercase">Scanned</span>';
                } else {
                    return '<span class="badge badge badge-warning text-uppercase">Unscanned</span>';
                }
            }
        },
        {
            "targets": 4,
            "data": "usedByTemplate",
            "render": function (data, type, row, meta) {
                if (row.usedByTemplate) {
                    return '<span class="badge badge badge-success text-uppercase">Yes</span>';
                } else {
                    return '<span class="badge badge badge-warning text-uppercase">No</span>';
                }
                
            }
        },
        {
            "targets": 5,
            "data": "created_at",
            "render": function (data, type, row, meta) {
                return row.created_at;
            }
        },
        {
            "targets": 6,
            "data": "id",
            "render": function (data, type, row, meta) {
                return '<button data-id="' + row.id + '" class="show-docData btn btn-outline-primary btn-xs" ><i class="fa fa-eye" aria-hidden="true"></i> View Data</button> <button data-id="' + row.id + '" class="scan-doc btn btn-outline-primary btn-xs" ><i class="fa fa-cogs" aria-hidden="true"></i> Extract Data</button> ' + '<button class="add-template btn btn-outline-primary btn-xs" data-docid="' + row.id + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Select Template</button> ' + ' <button data-id="' + row.id + '" class="delete-doc btn btn-outline-danger btn-xs" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>';
            }
        },
    ],
    searching: false,
    ordering: false,
    lengthChange: false,
    paging: false,
    autoWidth: false
});

$('#documents-data').on('click', 'tbody tr .add-template', function () {
    var templateData = [{value:'',text:'No Template'}];
    var formSelectTemplate;
    var docId = $(this).data('docid');
    axios.get('/api/templates')
    .then(function (response) {
        _.forEach(response.data.data, function (value) {
            templateData.push({
                value: value.id,
                text: value.template_name
            });
        });
        Bootbox.prompt({
            title: 'Select template',
            inputType: 'select',
            value: '',
            inputOptions: templateData,
            buttons: {
                cancel: {
                    label: 'Cancel',
                    className: 'btn-default'
                },
                confirm: {
                    label: 'Save',
                    className: 'btn-primary'
                }
            },
            callback: function (result) {
                if (typeof result !== "undefined" && result !== null) {
                    axios.post('/api/document/template/add', {
                        doc_id: docId,
                        template_id: result,
                        _token: $('meta[name="csrf-token"]').attr('content'),
                    }).then(function (response) {
                        console.log(response.data);
                        documentsTable.ajax.reload();
                        new Noty({
                            theme: 'bootstrap-v4',
                            text: response.data.message,
                            type: response.data.type,
                            timeout: timeOutNotif
                        }).show();
                    })
                    .catch(function (error) {
                        console.log(error.response);
                    });
                }
            }
        });
        
    })
    .catch(function (error) {
        console.log(error.response);
    });
});

$('#documents-data').on('click', 'tbody tr .scan-doc', function () {
    axios.post('/api/document/scan', {
            doc_id: parseInt($(this).attr('data-id')),
            _token: $('meta[name="csrf-token"]').attr('content'),
        }).then(function (response) {
            console.log(response.data);
            documentsTable.ajax.reload();
            new Noty({
                theme: 'bootstrap-v4',
                text: response.data.message,
                type: response.data.type,
                timeout: timeOutNotif
            }).show();
        })
        .catch(function (error) {
            console.log(error.response);
        });
});

$('#documents-data').on('click', 'tbody tr .delete-doc', function () {
    axios.delete('/api/document/delete', {
        params: {
            doc_id: $(this).attr('data-id'),
            _token: $('meta[name="csrf-token"]').attr('content'),
        }
    }).then(function (response) {
        console.log(response.data);
        documentsTable.ajax.reload();
        new Noty({
            theme: 'bootstrap-v4',
            text: response.data.message,
            type: response.data.type,
            timeout: timeOutNotif
        }).show();
    })
    .catch(function (error) {
        console.log(error.response);
    });
});

$('#documents-data').on('click', 'tbody tr .show-docData', function () {
    var doc_id = parseInt($(this).attr('data-id'));
    axios.get('/api/document/' + doc_id + '/data')
        .then(function (response) {
            if (response.data.data.document_data !== null) {
                var a = response.data.data.document_data;
                a = a.reduce((acc, i) => {
                    // Use Object.keys to get access to the key names.
                    Object.keys(i).map((key) => {
                        // Append item onto the accumulator using its key and value
                        // Warning: this will overwrite any existing `acc[key]` entries of the
                        // same `key` value.
                        acc[key] = i[key];
                    })
                    return acc;
                    // This last Object here is what we start with when reduce runs the first time.
                }, {});
                Bootbox.dialog({
                    title: JSON.stringify(response.data.data.document_data[0].document_name),
                    message: '<div class="row"><div class="col-md-7"><img src="' + response.data.data.document_image_path + '"></div><div class="col-md-3"><samp>' + JSON.stringify(a) + '</samp></div></div>',
                    className: 'modal-fullscreen'
                });
            } else{
                Bootbox.dialog({
                    title: JSON.stringify(response.data.data.document_data[0].document_name),
                    message: '<div class="row"><div class="col-md-7"><img src="' + response.data.data.document_image_path + '"></div><div class="col-md-3"><samp>No Data</samp></div></div>',
                    className: 'modal-fullscreen'
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        });
});

var templateTable = $('#templates-data').DataTable({
    ajax: '/api/templates',
    columns: [
        { data: 'template_name' },
        { data: 'created_at' },
    ],
    columnDefs: [
    {
        "targets": 2,
        "data": "id",
        "render": function (data, type, row, meta) {
            if (row.hasTemplateRule === false) {
                return '<a href="template/' + row.id + '/rule/create"><button data-id="' + row.id + '" class="show-docData btn btn-outline-primary btn-xs" ><i class="fa fa-plus" aria-hidden="true"></i> Add Parsing Rule</button></a> <button data-id="' + row.id + '" class="delete-template btn btn-outline-danger btn-xs" ><i class="fa fa-trash" aria-hidden="true"></i> delete</button>';
            }
            return '<a href="template/' + row.id + '/rule/create"><button data-id="' + row.id + '" class="show-docData btn btn-outline-primary btn-xs" ><i class="fa fa-plus" aria-hidden="true"></i> Add Parsing Rule</button></a> <a href="template/' + row.id + '"><button data-id="' + row.id + '" class="show-docData btn btn-outline-primary btn-xs" ><i class="fa fa-eye" aria-hidden="true"></i> View Parsing Rule</button></a> <button data-id="' + row.id + '" class="delete-template btn btn-outline-danger btn-xs" ><i class="fa fa-trash" aria-hidden="true"></i> delete</button>';
        }
    }, ],
    searching: false,
    ordering: false,
    lengthChange: false,
    paging: false,
    autoWidth: false
});

$('#templates-data').on('click', 'tbody tr .delete-template', function () {
    var id = $(this).attr('data-id');
    axios.delete('/api/template/delete', {
            params: {
                id: $(this).attr('data-id'),
                _token: $('meta[name="csrf-token"]').attr('content'),
            }
        }).then(function (response) {
            console.log(response);
            templateTable.ajax.reload();
            new Noty({
                theme: 'bootstrap-v4',
                text: response.data.message,
                type: response.data.type,
                timeout: timeOutNotif
            }).show();
        })
        .catch(function (error) {
            console.log(error.response);
        });
});

var rulesTable = $('#rules-data').DataTable({
    ajax: '/api/template/' + template_id + '/rules',
    columns: [
        { data: 'rule_name' },
        { data: 'created_at' },
        { data: 'id' },
    ],
    columnDefs: [
        // {
        //     "targets": 0,
        //     "data": "rule_name",
        //     "render": function (data, type, row, meta) {
        //         return '<a href="rule/' + row.id + '">' + data + '</a>';
        //     }
        // },
        {
            "targets": 2,
            "data": "id",
            "render": function (data, type, row, meta) {
                return '<a href="/documents/template/rule/' + row.id + '/edit"><button data-id="' + row.id + '" class="show-docData btn btn-outline-primary btn-xs" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</button></a> ' + ' <button data-id="' + row.id + '" class="delete-rule btn btn-outline-danger btn-xs" ><i class="fa fa-trash" aria-hidden="true"></i> delete</button>';
            }
        },
    ],
    searching: false,
    ordering: false,
    lengthChange: false,
    paging: false,
    autoWidth: false
});



$('#rules-data').on('click', 'tbody tr .delete-rule', function () {
    var id = $(this).attr('data-id');
    axios.delete('/api/rule/delete', {
        params: {
            id: $(this).attr('data-id'),
            _token: $('meta[name="csrf-token"]').attr('content'),
        }
    }).then(function (response) {
            console.log(response);
            $('#new-template').trigger("reset");
            rulesTable.ajax.reload();
            new Noty({
                theme: 'bootstrap-v4',
                text: response.data.message,
                type: 'success',
                timeout: timeOutNotif
            }).show();
        })
        .catch(function (error) {
            console.log(error.response);
        });
});

$('.document-id').change(function () {
    var docId = this.value;
    var img;
    axios.get('/api/document/' + docId)
        .then(function (response) {
            img = response.data[0].document_image_path;
            $('#select-doc-first').hide();
            $('#doc-image')[0].src = img;
        })
        .catch(function (error) {
            console.log(error);
            $('#select-doc-first').show();
            $('#doc-image')[0].src = '';
        });
});

$('#new-template').submit(function (e) {
    e.preventDefault();
    axios.post('/api/template/new', {
        template_name: $('.template-name').val(),
        document_id: $('.document-id').val(),
        _token: $('#csrf-token').val(),
        _userid: $('#user-id').val()
    }).then(function (response) {
            console.log(response);
            $('#new-template').trigger("reset");
            new Noty({
                theme: 'bootstrap-v4',
                text: response.data.message,
                type: 'success',
                timeout: timeOutNotif
            }).show();
        })
        .catch(function (error) {
            console.log(error);
        });
});

$('.save-rule').on('click',function(){
    var type = $(this).data('type');
    var url = '/api/rule/post';
    var data = {};
    colCords = [];
    if($('.rule-type').val() === 'tabular_data') {
        if ($('.separator').length === 0) {
            new Noty({
                theme: 'bootstrap-v4',
                text: 'Column is not defined, at least 2 colums',
                type: 'error',
                timeout: timeOutNotif
            }).show();
            return false;
        }
    }
    $('.separator').each(function () {
        colCords.push($(this).data('column'));
    });
    if (type === 'create') {
        data = {
            _type: type,
            rule_name: $('#rule-name').val(),
            cord_x: $('#cord-x').val(),
            cord_y: $('#cord-y').val(),
            cord_width: $('#cord-w').val(),
            cord_height: $('#cord-h').val(),
            _templateid: $('#template-id').val(),
            _userid: $('#user-id').val(),
            _token: $('#csrf-token').val(),
            rule_type: $('.rule-type').val(),
            height: height,
            column_cords: _.sortBy(colCords)
        }
    } else {
        data = {
            _type: type,
            rule_name: $('#rule-name').val(),
            cord_x: $('#cord-x').val(),
            cord_y: $('#cord-y').val(),
            cord_width: $('#cord-w').val(),
            cord_height: $('#cord-h').val(),
            rule_id: $('#rule-id').val(),
            _templateid: $('#template-id').val(),
            _ruleid: $('#rule-id').val(),
            _userid: $('#user-id').val(),
            _token: $('#csrf-token').val(),
            rule_type: $('.rule-type').val(),
            height: height,
            column_cords: _.sortBy(colCords)
        }
    }
    axios.post(url, data).then(function (response) {
        console.log(response);
        $('#form-rule').trigger("reset");
        colArray = [];
        colCords = [];
        resetCrop();
        new Noty({
            theme: 'bootstrap-v4',
            text: response.data.message,
            type: 'success',
            timeout: timeOutNotif
        }).show();
        setTimeout(function(){
            window.location.reload();
        },1000);
    })
    .catch(function (error) {
        console.log(error.response);
    });
})

var crop = $('.img-container > img');
crop.cropper({
    viewMode: 1,
    dragMode: false,
    checkCrossOrigin: false,
    toggleDragModeOnDblclick: false,
    zoomable: false,
    minCropBoxHeight: 10,
    crop: function (event) {
        $('#cord-x').val(event.detail.x.toFixed());
        $('#cord-y').val(event.detail.y.toFixed());
        $('#cord-w').val(event.detail.width.toFixed());
        $('#cord-h').val(event.detail.height.toFixed());
    }
});

var cropEdit = $('.img-container-edit > img');
cropEdit.cropper({
    viewMode: 1,
    dragMode: false,
    checkCrossOrigin: false,
    toggleDragModeOnDblclick: false,
    zoomable: false,
    minCropBoxHeight: 10,
    data: {
        x: parseInt($('#cord-x').val()),
        y: parseInt($('#cord-y').val()),
        width: parseInt($('#cord-w').val()),
        height: parseInt($('#cord-h').val()),
    },
    crop: function (event) {
        $('#cord-x').val(event.detail.x.toFixed());
        $('#cord-y').val(event.detail.y.toFixed());
        $('#cord-w').val(event.detail.width.toFixed());
        $('#cord-h').val(event.detail.height.toFixed());
    }
});

var resetCrop = function(){
    crop.cropper('destroy');
    crop.cropper({
        viewMode: 1,
        dragMode: false,
        checkCrossOrigin: false,
        toggleDragModeOnDblclick: false,
        zoomable: false,
        minCropBoxHeight: 10,
    });
}

var ZoomScale = function (container, image) {
    var c = container, i = image;
    var cw = c.width(), ch = c.height();
    var iw = i[0].naturalWidth, ih = i[0].naturalHeight;
    var cr = GetRatio(cw, ch), ir = GetRatio(iw, ih);
    var z;
    if (cr != ir) {
        z = Math.max(cw, ch) / Math.min(iw, ih);
    }
    if (cr == ir) {
        if (cw / iw < ch / ih)
            z = ch / ih;
        else
            z = cw / iw;
    }
    return z;
}

var GetRatio = function (w, h) {
    if (w / h > 1) return 1;
    if (w / h < 1) return -1;
    return 0;
}

function pixToPoint(pix, dpi) {
    return pix * 72 / dpi;
}

function pointToPix(pix, dpi) {
    return pix / 72 / dpi;
}

function realHeight(real_height, y) {
    return pixToPoint(real_height, 100) - pixToPoint(y, 100);
}

function sumCoord(start, width) {
    return pixToPoint(start, 100) + pixToPoint(width, 100);
}

function sumRealHeightCoord(start, height) {
    return start + height;
}

var renderCol = function(element) {
    var html;
    var id = 0;
    if (currentCol > 0) {
        id = currentCol;
    }
    colArray.push(id);
    html = '<div id="col-' + currentCol + '" class="separator col_separator ui-draggable" data-index="' + currentCol + '" data-column=""><div class="col_separator_line"></div><div class="col_separator_delete" data-index="' + id + '"><i class="fa fa-fw fa-trash"></i></div><div class="col_separator_move" data-index="' + id + '"><i class="fa fa-fw fa-arrows-h"></i></div><div class="col_separator_move col_separator_move_bottom" data-index=""><i class="fa fa-fw fa-arrows-h"></i></div></div>';
    $(element).append(html);
    currentCol++;
}

function dragCol(element) {
    element = $(element);
    element.draggable({
        axis: "x",
        containment: container,
        stop: function () {
            $(this).attr('data-column', coordinates(this)); //setter
        }
    });
}

var coordinates = function (element) {
    element = $(element);
    var left = element.position().left;
    return left.toFixed();
}

$('.col_separator_add').click(function(e){
    e.preventDefault();
    renderCol(container);
    dragCol('.separator');
});

function renderEditData() {
    if ($('.rule-type').val() == 'tabular_data') {
        $('.col_separator_add').show();
    } else {
        $('.col_separator_add').hide();
    }
    var html = '';
    var id = 0;
    var colCord = $('#cord-cols').data('cord');
    _.forEach(colCord, function(value){
        html += '<div id="col-' + currentCol + '" class="separator col_separator ui-draggable" data-index="' + currentCol + '" data-column="' + value + '" style="left: ' + value + 'px; top: 0px;"><div class="col_separator_line"></div><div class="col_separator_delete" data-index="' + currentCol + '"><i class="fa fa-fw fa-trash"></i></div><div class="col_separator_move" data-index="' + currentCol + '"><i class="fa fa-fw fa-arrows-h"></i></div><div class="col_separator_move col_separator_move_bottom" data-index=""><i class="fa fa-fw fa-arrows-h"></i></div></div>';
        currentCol++;
    })
    $('.img-container-edit').append(html);
    dragCol('.separator');
}

$(container).on('click', '.separator .col_separator_delete ', function () {
    var parent = $(this).parent('div')[0].id;
    var id = $('#' + parent).data('index');
    var cord = $('#' + parent).data('column');
    _.pull(colArray, id);
    $('#' + parent).remove();
});

$('.rule-type').on('change', function () {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if (valueSelected == 'tabular_data') {
        $('.col_separator_add').show();
    } else {
        $('.col_separator_add').hide();
    }
});