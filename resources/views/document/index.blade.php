@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4 float-right">
                        <div class="col-md-12">
                        <a href="{{route('document.upload')}}"><button type="button" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</button></a>
                        </div>
                    </div>
                    <table id="documents-data" class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Document Name</th>
                                <th>Parsing Template</th>
                                <th>Status</th>
                                <th>Used by Template</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
