@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="dropzone" action="{{route('document.postUpload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{-- <div class="form-group">
                            <label for="exampleFormControlFile1">Select Document</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Upload Document</button>
                        </div> --}}
                    </form>
                    <div class="form-group mt-4 pull-right">
                        <a href="{{route('document.index')}}" class="btn btn-primary">Finish Upload</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
