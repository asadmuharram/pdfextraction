@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4 float-right">
                        <div class="col-md-12">
                        <a href="{{route('rule.create',['id' => $template->id])}}"><button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> New Parsing Rule</button></a>
                        </div>
                    </div>
                    <table id="rules-data" class="table" data-template-id="{{$template->id}}">
                        <thead>
                            <tr>
                                <th>Rule Name</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
