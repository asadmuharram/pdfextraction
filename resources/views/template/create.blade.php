@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h3 id="select-doc-first" class="text-center">Please select document first</h3>
                    <img id="doc-image" src="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">New Template</div>
                <div class="card-body">
                    <form id="new-template" method="POST">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <input type="hidden" name="_userid" id="user-id" value="{{ Auth::user()->id }}" />
                        <div class="form-group">
                            {{ Form::label('Select Document', 'Choose document for template') }}
                            {{ Form::select('document_id', $documents, null, ['placeholder' => 'Select document','class' => 'document-id form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Template Name</label>
                            <input type="text" class="template-name form-control" placeholder="Enter template name" name="template_name">
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        <a href="{{route('template.index')}}"><button type="button" class="btn btn-link">Back</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
