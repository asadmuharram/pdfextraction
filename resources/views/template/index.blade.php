@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4 float-right">
                        <div class="col-md-12">
                            <a href="{{route('template.create')}}"><button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> New Template</button></a>
                        </div>
                    </div>
                    <table id="templates-data" class="table">
                        <thead>
                            <tr>
                                <th>Template Name</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
