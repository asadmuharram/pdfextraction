@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-9">
            <div class="card">
                <div class="card-body">
                    <div class="img-container-edit">
                        <div class="col_separator_add tooltip_trigger" title="Add new column separator">
                            <i class="fa fa-fw fa-plus"></i>
                        </div>
                        <img id="doc-img" class="img" src="{{$rule->template->document->document_image_path}}" data-height="{{$rule->template->document->document_height}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Edit Rule</div>
                <div class="card-body">
                    <form id="form-rule" method="POST" data-type="edit">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <input type="hidden" name="_userid" id="user-id" value="{{ Auth::user()->id }}" />
                        <input type="hidden" name="_templateid" id="template-id" value="{{ $rule->template->id }}" />
                        <input type="hidden" name="_ruleid" id="rule-id" value="{{ $rule->id }}" />
                        <input id="cord-x" type="hidden" name="cord_x" value="{{ $rule->cord_x }}">
                        <input id="cord-y" type="hidden" name="cord_y" value="{{ $rule->cord_y }}">
                        <input id="cord-w" type="hidden" name="cord_width" value="{{ $rule->cord_width }}">
                        <input id="cord-h" type="hidden" name="cord_height" value="{{ $rule->cord_height }}">
                        <input id="cord-cols" data-cord="{{ json_encode($rule->column_cords) }}" type="hidden">
                        <div class="form-group">
                            <label>Rule Name</label>
                            <input id="rule-name" type="text" class="template-name form-control" placeholder="Enter rule name" name="rule_name" value="{{ $rule->rule_name }}">
                        </div>
                        <div class="form-group">
                            {{ Form::label('Select Type', 'Choose type of data') }}
                            {{ Form::select('rule_type',array('single_data' => 'Single Data','tabular_data' => 'Tabular Data'), $rule->type, ['placeholder' => 'Select type','class' => 'rule-type form-control','disabled' => 'disabled']) }}
                        </div>
                        <button type="button" data-type="edit" class="save-rule btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        <a href="{{route('template.detail',['id' => $rule->template->id])}}"><button type="button" class="btn btn-link">back</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
