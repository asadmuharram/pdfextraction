@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-9">
            <div class="card">
                <div class="card-body" align="center">
                    <div class="img-container">
                        <div class="col_separator_add tooltip_trigger" title="Add new column separator">
                            <i class="fa fa-fw fa-plus"></i>
                        </div>
                        <img id="doc-img" class="img" src="{{$template->document->document_image_path}}" data-height="{{$template->document->document_height}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">New Parsing Rule</div>
                <div class="card-body">
                    <form id="form-rule" method="POST" data-type="create">
                        <input type="hidden" id="mode" name="mode" value="table">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <input type="hidden" name="_userid" id="user-id" value="{{ Auth::user()->id }}" />
                        <input type="hidden" name="_templateid" id="template-id" value="{{ $template->id }}" />
                        <input id="cord-x" type="hidden" class="form-control" name="cord_x">
                        <input id="cord-y" type="hidden" class="form-control" name="cord_y">
                        <input id="cord-w" type="hidden" class="form-control" name="cord_width">
                        <input id="cord-h" type="hidden" class="form-control" name="cord_height">
                        <div class="form-group">
                            <label>Rule Name</label>
                            <input id="rule-name" type="text" class="template-name form-control" placeholder="Enter rule name" name="rule_name">
                        </div>
                        <div class="form-group">
                            {{ Form::label('Select Type', 'Choose type of data') }}
                            {{ Form::select('rule_type',array('single_data' => 'Single Data','tabular_data' => 'Tabular Data'), null, ['placeholder' => 'Select type','class' => 'rule-type form-control']) }}
                        </div>
                        {{-- <div class="form-group">
                            <label >Position</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">left</span>
                                </div>
                                <input id="coord-left" type="text" class="form-control" name="cord_left">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">top</span>
                                </div>
                                <input id="coord-top" type="text" class="form-control" name="cord_top">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">right</span>
                                </div>
                                <input id="coord-right" type="text" class="form-control" name="cord_right">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">bottom</span>
                                </div>
                                <input id="coord-bottom" type="text" class="form-control" name="cord_bottom">
                            </div>
                        </div> --}}
                        <button type="button" data-type="create" class="save-rule btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                        <a href="{{route('template.detail',['id' => $template->id])}}"><button type="button" class="btn btn-link">Back</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
