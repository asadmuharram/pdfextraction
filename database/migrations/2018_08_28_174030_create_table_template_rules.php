<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTemplateRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rule_name');
            $table->string('cord_x')->nullable();
            $table->string('cord_y')->nullable();
            $table->string('cord_width')->nullable();
            $table->string('cord_height')->nullable();
            $table->string('cord_left')->nullable();
            $table->string('cord_top')->nullable();
            $table->string('cord_right')->nullable();
            $table->string('cord_bottom')->nullable();
            $table->integer('template_id')->unsigned();
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
            $table->string('xml_path');
            $table->timestamps();
        });
        // Schema::table('template_rules', function (Blueprint $table) {
        //     $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_rules', function (Blueprint $table) {
            $table->dropForeign(['template_id']);
            $table->dropColumn('template_id');
        });
        Schema::dropIfExists('template_rules');
    }
}
