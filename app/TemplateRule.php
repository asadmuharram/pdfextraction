<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use App\Http\Controllers\TemplateRuleController;

class TemplateRule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_rules';
    public $appends = ['column_cords'];

    protected $casts = [
        'column_cords' => 'array',
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function template() {
        return $this->belongsTo('App\Template');
    }

    public function getColumnCordsAttribute(){
        if($this->attributes['column_cords'] !== null){
            $cords = json_decode($this->attributes['column_cords'], true);
            foreach($cords as $cord) { 
                $newCords[] = TemplateRuleController::pointToPixel($cord,100);
            };
            return $newCords;
        } else {
            return $this->attributes['column_cords'];
        }
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->diffForHumans();
    }
    
}
