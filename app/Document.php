<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Template;
use Carbon;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document_name', 'document_path', 'status',
    ];

    protected $casts = [
        'document_data' => 'array',
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function template() {
        return $this->belongsToMany('App\Template')->using('App\DocumentTemplate')->first();
    }

    public function templates() {
        return $this->belongsToMany('App\Template')->using('App\DocumentTemplate')->withTimestamps();
    }

    public function isHasTemplate() {
        if($this->belongsToMany('App\Template')->using('App\DocumentTemplate')->count() > 0){
            return true;
        } 
        return false;
    }

    public function usedByTemplate()
    {
        if($this->hasMany('App\Template')->count() > 0){
            return true;
        } 
        return false;
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->diffForHumans();
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
    
}
