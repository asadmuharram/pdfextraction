<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DocumentTemplate extends Pivot
{
    protected $table = 'document_template';
}
