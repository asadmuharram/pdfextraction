<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_name', 'document_id', 'user_id'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function document() {
        return $this->belongsTo('App\Document');
    }

    public function documents() {
        return $this->belongsToMany('App\Document')->using('App\DocumentTemplate');
    }

    public function templateRules() {
        return $this->hasMany('App\TemplateRule');
    }

    public function isUsedByDoc()
    {
        if($this->belongsToMany('App\Document')->using('App\DocumentTemplate')->count() > 0){
            return true;
        } 
        return false;
    } 

    public function isHasTemplateRule() {
        if(TemplateRule::where('template_id',$this->id)->count() > 0){
            return true;
        } 
        return false;
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->diffForHumans();
    }
}
