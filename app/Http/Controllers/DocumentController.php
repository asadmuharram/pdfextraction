<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Document;
use Carbon;
use App\Functions\PdfToText;
use File;

class DocumentController extends Controller
{
    protected $img_dpi;

    public function __construct() 
    {
        $this->img_dpi = config('app.dpi');
    }

    public function index()
    {
        return view('document.index');
    }

    public function getAll()
    {
        $documents = Document::with('templates')->get();
        $documents->map(function ($document) { 
            $document['hasTemplate'] = $document->isHasTemplate();
            $document['usedByTemplate'] = $document->usedByTemplate();
            return $document;
        });
        return response()->json(array('data' => $documents), 200);
    }

    public function getDocImgById($id)
    {
        $document = Document::where('id',$id)->select('document_image_path')->get();
        return response()->json($document, 200);
    }

    public function upload()
    {
        return view('document.upload');
    }

    public function postUpload(Request $request)
    {
        $time = $this->setTime();
        $file = $request->file('file'); 
        $extension = $file->getClientOriginalExtension();
        $filename = str_random(5).date_format($time,'d').rand(1,9).date_format($time,'h').".".$extension;
        $directory = $this->fileDirectory();
        $upload_success = $file->storeAs($directory, $filename, 'public');
        $upload_path = '/uploads/'. $directory .'/'. $filename;
        $converImg = $this->convertToImg($file,$filename,public_path('/uploads'). '/'. $directory,$this->img_dpi);
        if ($upload_success) {
            $document = new Document();
            $document->document_name = $file->getClientOriginalName();
            $document->document_image_path = $converImg['image_path'];
            $document->document_path = $upload_path;
            $document->document_width = $converImg['width'];
            $document->document_height = $converImg['height'];
            $document->document_dpi = $this->img_dpi;
            $document->status = 'unscanned';
            $document->user_id = Auth::user()->id;
            $document->save();
            return response()->json($converImg['image_path'], 200);
        }
        else {
            return response()->json('error', 400);
        }
    }

    public function scanDoc(Request $request)
    {
        $data = array();
        $document = Document::find($request->input('doc_id'));
        if($document->isHasTemplate()){
            if($document->templates[0]->isHasTemplateRule()){
                $templateRules = $document->templates[0]->templateRules;
                $file = public_path($document->document_path);
                $data[] = array(
                    'document_name' => $document->document_name
                );
                foreach($templateRules as $rule){
                    if($rule->type == 'single_data'){
                        $pdf =  new PdfToText($file, PdfToText::PDFOPT_DEBUG_SHOW_COORDINATES );
                        $pdf->SetCaptures(public_path($rule->xml_path));
                        $capture = $pdf->getCaptures();
                        $data[] = array(
                            $rule->rule_name => $capture->data[1]
                        );
                    } else{
                        $data[] = array(
                            $rule->rule_name => $this->parsingTable($rule,$file)
                        );
                    }
                }
                $document->status = 'scanned';
                $document->document_data = $data;
                $document->save();
                return response()->json(array('message' => 'Document has succesfully scanned','data' => $data,'type' => 'success'), 200);
            }
            return response()->json(array('message' => 'This document template need to have rule before scan','data' => $data, 'type' => 'error'), 200);
        }
        return response()->json(array('message' => 'This document need to have template before scan','data' => $data, 'type' => 'error'), 200);
    }

    public function showData($id)
    {
        if($document = Document::where('id',$id)->first()){
            return response()->json(array('data' => $document), 200);
        } else {
            return response()->json(array('data' => null), 200);
        }
    }

    public function addTemplate(Request $request)
    {
        $document = Document::find($request->input('doc_id'));
        $document->templates()->detach();
        if($request->input('template_id') !== ''){
            $document->templates()->attach($request->input('template_id'));
        }
        return response()->json(array('message' => 'Template has successfully changed','type' => 'success'), 200);
    }

    public function delete(Request $request)
    {
        $document = Document::where('id',$request->input('doc_id'))->first();
        if(!$document->usedByTemplate()){
            File::delete(public_path($document->document_image_path));
            File::delete(public_path($document->document_path));
            // Delete rule
            // if(!empty($document->template())){
            //     foreach($document->template()->templateRules as $rule){
            //         File::delete(public_path($rule->xml_path));
            //     }
            // }
            $document->templates()->detach();
            if($document->delete()){
                return response()->json(array('message' => 'Document has successfully deleted','type' => 'success'), 200);
            } else {
                return response()->json(array('message' => 'Error deleting document', 'type' => 'error'), 200);
            }
        }
        return response()->json(array('message' => 'This document is being used as a template','type' => 'error'), 200);
    }

    private function parsingTable($rule,$file)
    {   
        $pdf =  new PdfToText($file, PdfToText::PDFOPT_DEBUG_SHOW_COORDINATES );
        $pdf->SetCaptures(public_path($rule->xml_path));
        $capture = $pdf->getCaptures();
        $index = 1 ;
        $array = array();
        $array2 = array();
        $array3 = array();
        $result = array();

        foreach ($capture->data as $line) {
            $array[] = $line;
        }
        array_pop($array);
        foreach($array as $key => $value)
        {
            $array2[$key] = (array) $value;
        }

        foreach($array2[0] as $data)
        {
            $array3[] = $data;
        }

        foreach($array3 as $key => $record)
        {
            $result[] = explode("\n", $array3[$key]);
        }

        $itemList = array();
        $max = count($result[0]) - 1;
        $output = array_reduce($result, function (array $out, array $items) use ($max) {
                $header = array_shift($items);
                $redundant = array_splice($items, $max);
                foreach ($items as $idx => $value) {
                    $out[$idx][$header] = $value;
                }
                foreach ($redundant as $value) {
                    $out[$idx][$header] .= ' ' . $value;
                }
                return $out;
            },[]
        );

        return $output;
    }

    private function removeFileExt($file)
    {
        return substr(basename($file),0,strpos(basename($file),'.'));
    }

    private function fileDirectory()
    {
        $time = $this->setTime();
        return Auth::user()->id. '/' .date_format($time, 'Y') . '/' . date_format($time, 'm');
    }

    private function convertToImg($file,string $filename,string $filepath, int $dpi)
    {
        $imgFormat = 'jpeg';
        $backgroundColor = 'white';
        $imgPath = $filepath.'/'.$this->removeFileExt($filename).'.'.$imgFormat;

        try {
            $img = new \Imagick();
            $img->setResolution($dpi,$dpi);
            $img->readImage($file);
            $img->setImageFormat($imgFormat);
            $img->setImageBackgroundColor($backgroundColor);
            $img->setImageAlphaChannel(11);
            $img->writeImage($imgPath);
            return array(
                'width' => $img->getImageWidth(),
                'height' => $img->getImageHeight(),
                'image_path' => strstr($imgPath,'/uploads')
            );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    private function setTime()
    {
        return Carbon::now();
    }
}
