<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\Document;
use App\TemplateRule;
use Auth;
use \FluidXml\FluidXml;
use Carbon;
use File;

class TemplateRuleController extends Controller
{
    protected $img_dpi;

    const DEFAULT_DPI = 72;

    public function __construct() 
    {
        // Image DPI can be changed on config/app.php
        $this->img_dpi = config('app.dpi');
    }

    public static function getDefaultDpi() 
    { 
        return self::DEFAULT_DPI; 
    }

    public function getAll()
    {
        $rules = TemplateRule::all();
        return response()->json(array('data' => $rules), 200);
    }

    public function create($id)
    {
        return view('templaterule.create',[
            'template' => Template::find($id)
        ]);
    }

    public function edit($id)
    {
        return view('templaterule.edit',[
            'rule' => TemplateRule::find($id)
        ]);
    }

    public function delete(Request $request)
    {
        $rule = TemplateRule::find($request->input('id'));
        File::delete(public_path($rule->xml_path));
        $rule->delete();
        return response()->json(array('message' => 'Rule succesfully deleted'), 200);
    }

    public function post(Request $request)
    {
        $time = $this->setTime();
        $xmlname = str_random(5).date_format($time,'d').rand(1,9).date_format($time,'h').".xml";
        $path = public_path('/uploads').'/'.$request->input('_userid').'/xml';

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $fullpath = '/uploads/'.$this->xmlDirectory($request->input('_userid')).'/'.$xmlname;

        if($request->input('_type') == 'create'){
            $rule = new TemplateRule();
        } else {
            $rule = TemplateRule::find($request->input('_ruleid'));
        }
        
        $rule->rule_name = $request->input('rule_name');
        $bottomCord = $request->input('cord_y') + $request->input('cord_height');

        //Original Cord from cropper
        $rule->cord_x = $request->input('cord_x');
        $rule->cord_y = $request->input('cord_y');
        $rule->cord_width = $request->input('cord_width');
        $rule->cord_height = $request->input('cord_height');
        
        //Real cordinate for xml
        $rule->cord_left = round($this->pixelToPoint($request->input('cord_x'),$this->img_dpi));
        $rule->cord_top = round($this->realHeight($request->input('height'),$request->input('cord_y')));
        $rule->cord_right = round($this->sumCord($request->input('cord_x'),$request->input('cord_width')));
        $rule->cord_bottom = round($this->realHeight($request->input('height'), $bottomCord));

        $rule->type = $request->input('rule_type');

        if($rule->type == 'tabular_data'){
            $rule->column_cords = array_map(function($value){
                return $this->pixelToPoint($value,$this->img_dpi);
            },$request->input('column_cords'));
        }

        $rule->template_id = $request->input('_templateid');
        if($request->input('_type') == 'edit'){
            File::delete(public_path($rule->xml_path));
        }
        $rule->xml_path = $fullpath;
        $rule->save();
        
        if($rule->type == 'tabular_data'){
            $tableHeight = $rule->cord_top - $rule->cord_bottom;
            $this->createColXml($rule->cord_left,$rule->cord_right,$rule->cord_top,$tableHeight,$rule->cord_bottom,$rule->column_cords,$path,$xmlname);
        } else {
            $this->createRectXml($rule->cord_left,$rule->cord_top,$rule->cord_right,$rule->cord_bottom,$path,$xmlname);
        }

        return response()->json(array('message' => 'Rule succesfully created'), 200);
    }

    private function setTime()
    {
        return Carbon::now();
    }

    private function createRectXml($left,$top,$right,$bottom,$xmlpath,$name)
    {
        $captures = new FluidXml(null, ['root' => 'captures', 'version' => '1.0', 'encoding' => 'UTF-8']);
        $captures->add('rectangle', true, ['name' => 'data'])->add('page', ['number' => 1, 'left' => $left, 'top' => $top, 'right' => $right, 'bottom' => $bottom ]);
        $captures->save($xmlpath.'/'.$name);
        return;
    }

    private function createColXml($rectLeft,$rectRight,$top,$height,$bottom,array $colCords,$xmlpath,$name)
    {
        $i = 1; // Column name iteration
        $captures = new FluidXml(null, ['root' => 'captures', 'version' => '1.0', 'encoding' => 'UTF-8']);
        $captures->add('lines', true, ['name' => 'data', 'separator' => '\n','default' => 'undefined'])->add('page', ['number' => 1, 'top' => $top, 'height' => $height, 'bottom' => 0 ]);
        $lastArray = count($colCords) - 1;
        $captures->query('//captures/lines')->add('column',true,['name' => 'column0','left' => $rectLeft, 'right' => $colCords[0]]);
        foreach($colCords as $key => $cord){
            if($key < $lastArray){
                $captures->query('//captures/lines')->add('column',true,['name' => 'column'.$i,'left' => $cord, 'right' => $colCords[$key + 1]]);
                $i++;
            }
        }
        $captures->query('//captures/lines')->add('column',true,['name' => 'column'.$i,'left' => $colCords[$lastArray], 'right' => $rectRight]);
        $captures->save($xmlpath.'/'.$name);
        return;
    }

    private function xmlDirectory($userId)
    {
        return $userId.'/xml';
    }

    private function pixelToPoint($pixel, $dpi)
    {
        return $pixel * self::getDefaultDpi() / $dpi;
    }

    static function pointToPixel($pixel, $dpi)
    {
        return $pixel / self::getDefaultDpi() * $dpi;
    }

    private function realHeight($realHeight, $y)
    {
        return $this->pixelToPoint($realHeight,$this->img_dpi) - $this->pixelToPoint($y,$this->img_dpi);
    }

    private function sumCord($start,$width)
    {
        return $this->pixelToPoint($start,$this->img_dpi) + $this->pixelToPoint($width,$this->img_dpi);
    }

}
