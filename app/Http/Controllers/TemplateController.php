<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\Document;
use App\TemplateRule;
use Auth;
use File;

class TemplateController extends Controller
{
    public function index()
    {
        return view('template.index');
    }

    public function getAll()
    {
        $templates = Template::with('documents')->get();
        $templates->map(function ($template) {
            $template['hasTemplateRule'] = $template->isHasTemplateRule();
            return $template;
        });
        return response()->json(array('data' => $templates), 200);
    }

    public function getRuleById($id)
    {
        $rules = TemplateRule::where('template_id',$id)->get();
        return response()->json(array('data' => $rules), 200);
    }

    public function newTemplate()
    {
        return view('template.create',[
            'documents' => Document::pluck('document_name', 'id')
        ]);
    }

    public function detail($id)
    {
        return view('template.detail',[
            'template' => Template::find($id)
        ]);
    }

    public function post(Request $request)
    {
        $template = new Template();
        $template->template_name = $request->input('template_name');
        $template->document_id = $request->input('document_id');
        $template->user_id = $request->input('_userid');
        $template->save();
        return response()->json(array('message' => 'Template succesfully created'), 200);
    }

    public function delete(Request $request)
    {
        $template = Template::find($request->input('id'));
        if($template->isUsedByDoc()){
            return response()->json(array('message' => 'Template is being used by document', 'type' => 'error'), 200);
        } else {
            foreach($template->templateRules() as $rule){
                File::delete(public_path($rule->xml_path));
            }
            $template->delete();
            return response()->json(array('message' => 'Template succesfully deleted', 'type' => 'success'), 200);
        }
        
    }
}
