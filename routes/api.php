<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/documents', 'DocumentController@getAll')->name('document.getAll');
Route::get('/document/{id}', 'DocumentController@getDocImgById')->name('document.getDocImgById');
Route::post('/document/scan', 'DocumentController@scanDoc')->name('document.scan');
Route::post('/document/template/add', 'DocumentController@addTemplate')->name('document.addTemplate');
Route::get('/document/{id}/data', 'DocumentController@showData')->name('document.showData');
Route::delete('/document/delete', 'DocumentController@delete')->name('document.delete');

Route::get('/templates', 'TemplateController@getAll')->name('template.getAll');
Route::post('/template/new', 'TemplateController@post')->name('template.post');
Route::get('/template/{id}/rules', 'TemplateController@getRuleById')->name('template.getRuleById');
Route::delete('/template/delete', 'TemplateController@delete')->name('template.delete');

Route::delete('/rule/delete', 'TemplateRuleController@delete')->name('rule.delete');
Route::post('/rule/post', 'TemplateRuleController@post')->name('rule.post');