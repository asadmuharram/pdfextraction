<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Auth::routes();

Route::post('/documents/postupload', 'DocumentController@postUpload')->name('document.postUpload');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/documents', 'DocumentController@index')->name('document.index');
    Route::get('/documents/upload', 'DocumentController@upload')->name('document.upload');
    
    Route::get('/documents/templates', 'TemplateController@index')->name('template.index');
    Route::get('/documents/templates/create', 'TemplateController@newTemplate')->name('template.create');
    Route::get('/documents/template/{id}', 'TemplateController@detail')->name('template.detail');

    Route::get('/documents/template/{id}/rule/create', 'TemplateRuleController@create')->name('rule.create');
    Route::get('/documents/template/rule/{id}/edit', 'TemplateRuleController@edit')->name('rule.edit');
    Route::get('/documents/template/rule/{id}', 'TemplateRuleController@edit')->name('rule.edit');
});

// Route::get('/getApi', function(){
//     $user = App\User::find(1);
//     //Creating a token without scopes...
//     $token = $user->createToken('pdfextract')->accessToken;
// });